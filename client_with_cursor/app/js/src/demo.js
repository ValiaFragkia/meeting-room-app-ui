$(document).ready(function () {

	//---------------------------------------
	//#region Demo Button Clicks

	$('.demoBtn').click(function () {
	  $(this).toggleClass('checked')
	});

//go to page-dashboard
	$('.purple-btn1').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	 // PageTransitions.goToPage(3, 'page2');
   PageTransitions.goToPage(1, 'page3');
	});

//go to page-meetings
	$('.purple-btn2').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(1, 'page1');
	});

//go to page-details
	$('.btn_grey1').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(1, 'page2');
	});

	//#endregion
	//---------------------------------------

  });
