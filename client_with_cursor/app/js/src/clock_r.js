function initial(){
	var startString = sessionStorage.timeStart;
	if (startString == null){
		startTimer();
	} else{
		timer();
	}
}

function startTimer(){
	var start = new Date();

	var startMillis = start.getTime();

	sessionStorage.timeStart = startMillis;
	timer();
}

function timer(){
	setInterval(function() { refresh(); }, 1000);

	 function refresh(){
	 	var date = new Date();

		var startString = sessionStorage.timeStart;
		var startMillis = parseInt(startString);

		var diff = date.getTime() - startMillis;

	 var time = document.getElementById('timediv');
	 var sec = 0;
	 var min = 0;
	 var h = 0;

	 sec = (Math.floor(diff/1000)) % 60;
	 min = (Math.floor(diff/(1000 * 60))) % 60;
	 h = (Math.floor(diff/(3600 * 1000))) % 24;

	 time.innerHTML = h+":"+min+":"+sec;

	 }
}
