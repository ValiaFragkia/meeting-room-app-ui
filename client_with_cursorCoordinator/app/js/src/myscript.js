function changeImage(img) {
    var image = document.getElementById(img);
    if (image.src.match("delete-white")) {
        image.src = "app/icons/plus-5-48.ico";
    }
    else {
        image.src = "app/icons/delete-white.png";
    }
}

function remove(elem) {
    elem.style.display = "none";
}

// krataei tin katastasi tou koumpiou add gia tous xristes
var addStatus=0;

/* Methodos gia to koumpi pou prosthetei xristes
   Elegxei ean to koumpi exei patithei gia prosthesi xristwn kai xeirizetai analoga ti tha emfanizetai */
function changeStatus(){
	var status=document.getElementById("add");
	if(status.src.match("add")){
		addStatus=1;
		status.src = "app/icons/plus-5-48.ico";
		showUsers();
	}else{
		addStatus=0;
		status.src = "app/icons/add.ico";
		updateUsers();
	}
}

// Emfanizei oli ti lista apo tous xristes kai epilegei poious tha prosthesei kai poious tha afairesei
function showUsers(){
	var i=1;
	for(i=1;i<=6;i++){
		var user=document.getElementById('user'+i);
		if(i<4){
			document.getElementById("set"+i).style.display = "none";
		}
		if(user.style.display.match("none")){
			user.style.display = "";
			document.getElementById("delete"+i).src="app/icons/plus-5-48.ico";
		}
		document.getElementById("mute"+i).style.display = "none";
		document.getElementById("tv"+i).style.display = "none";
	}
}

// Emfanizei tous xristes pou symmetexoun sto paron meeting
function updateUsers(){
	var i=1;
	for(i=1;i<=6;i++){
		var user=document.getElementById('user'+i);
		if(document.getElementById("delete"+i).src.match("plus-5-48")){
			user.style.display="none";
		}else{
			user.style.display = "";
			if(i<4){
				document.getElementById("set"+i).style.display = "";
			}
			document.getElementById("mute"+i).style.display = "";
			document.getElementById("tv"+i).style.display = "";
		}

		document.getElementById("delete"+i).src="app/icons/delete-white.png";
	}
}

/* Ean to koumpi delete patithei enw to addStatus den einai energo,
   o epilegmenos xristis diagrafetai, alliws to eikonidio allazei se sima tou add */
function action(i){
	user=document.getElementById("user"+i);
	if(addStatus===0){
		remove(user);
	}else{
		changeImage("delete"+i);
	}
}

function customImg(){
	var i;
	for(i=1;i<=16;i++){
		var element=document.getElementById("check"+i)
		element.style.margin="10% 0% 0% 14%";
		element=document.getElementById("dcheck"+i)
		element.style.margin="10% 0% 0% 14%";
	}

}
