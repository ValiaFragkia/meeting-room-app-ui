$(document).ready(function () {

	//---------------------------------------
	//#region Demo Button Clicks
  
	$('.demoBtn').click(function () {
	  $(this).toggleClass('checked')
	});
  
	$('.purple-btn1').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	 // PageTransitions.goToPage(3, 'page2');
	 PageTransitions.goToPage(61, 'page5');
	});
  
	$('.purple-btn2').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(60, 'page1');
	});

	$('.btnBook').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(61, 'page2');
	});
	
	$('.btnDetails').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(61, 'page3');
	});

	$('.btnStart').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(61, 'page4');
	});
	$('.btn_dashboard').click(function () {
	  // Tip: try other integers [1-67] at PageTransitions.goToPage function
	  // and see different animations on changing pages
	  PageTransitions.goToPage(60, 'page5');
	});
	//#endregion
	//---------------------------------------
  
  });