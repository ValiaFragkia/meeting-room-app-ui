//*******************************************
//  SOCKETS CONTROLLER
//*******************************************

var flag = true;
console.log("f"+flag);

var Sockets = (function () {

  function HandleMessages(e) {
    var data = jQuery.parseJSON(e.data);
    //data.type - the type of the message
    //data.message - the data sent from server

    //This function is called when receiving a message from the communication server.
    //First you have to check the message type to find what message the server sent.
    //You can then call/write the appropriate code - it is recommended to create a seperate file for each type of messages
    //You can have a prefix for messages that handle same type of data and then dispatch them using the postfix (e.g. hello:message1 , hello:message2)
    var msgType = data.type;
    // console.log('received a message ' + data.type);
    if (flag === true){
      if (msgType.includes('helloWorld')) {
        HelloWorldManager.DataHandler(data);
      }else if(msgType.includes("kinect/speech")){
        console.log("speech was received");
        console.log(data);
        if(data.message.speechValue==='WAIT'){
          // $('.demoBtn1').click(function () {
            // Tip: try other integers [1-67] at PageTransitions.goToPage function
            // and see different animations on changing pages
            controlVideo('PAUSE');
            flag = false;
            setTimeout(function(){ flag = true; }, 3000);
            // PageTransitions.goToPage(1, 'page2');
          // });
        } else if(data.message.speechValue==='PLAY') {
            controlVideo('PLAY');
            flag = false;
            setTimeout(function(){ flag = true; }, 3000);
        } 
      }else if(msgType.includes("kinect/gesture")){
        console.log("gesture was received");
        console.log(data);
        if(data.message.gestureType==='SwipeRight' || data.message.gestureType==='WaveRight'){
        // $('.demoBtn1').click(function () {
          // Tip: try other integers [1-67] at PageTransitions.goToPage function
          // and see different animations on changing pages
          plusDivs(+1);
          flag = false;
          setTimeout(function(){ flag = true; }, 3000);
          console.log("f"+flag);
        } else if(data.message.gestureType==='SwipeLeft' || data.message.gestureType==='WaveLeft' ){
          // $('.demoBtn1').click(function () {
            // Tip: try other integers [1-67] at PageTransitions.goToPage function
            // and see different animations on changing pages
            plusDivs(-1);
            flag = false;
            setTimeout(function(){ flag = true; }, 3000);
            console.log("f"+flag);
        }
      }else if(msgType.includes("sensors/linear")){
        // console.log('received a message from server that i dont handle with type:' + data.type);
        console.log(data);
        changeVolume(data.message.actualValue);
        
      }else{

      }
    }
  }



  class WSConnection {

    constructor() {
      this.socketConnection = null;
    }

    /**
     * Connect to socket server
     */
    Connect() {
      this.socketConnection = new WebSocket(GlobalConfig.Connections.Sockets, GlobalConfig.DeviceType);

      this.socketConnection.onopen = function () {
        console.info("Sockets connected!");
      };

      this.socketConnection.onerror = function (error) {
        console.info('Sockets Error: ' + error);
      };


      this.socketConnection.onmessage = HandleMessages;
    }

    /**
     * Disconnect from socket server
     */
    Disconnect() {
      this.socketConnection.disconnect();
      console.info('Sockets disconnected!');
    }

    /**
     * Send message to socket server
     *
     * @param {string} type - Type of message
     * @param {any} msg - Object to send as message
     */
    SendMessage(type, msg) {
      this.socketConnection.send(JSON.stringify({
        type: type,
        message: msg
      }));
    }

  }

  return new WSConnection();
})();
