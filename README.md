Purpose of this project is to design and implement a startup's Meeting Room App User Interface
for the coordinator and the local and remote participants.

Modalities are implemented, such as hand gestures recognition (swipe left and right) for changing the presentation slides
via Leap Motion Sensor, a cursor for the presentation via Leap Motion Sensor, regulating a linear Phidget Sensor for 
regulating the volume of the video playing, voice recognition ("play" and "wait") to start and stop a video via Kinect 
Sensor.  

It is written in Html, Javascript and Css. It uses the Bootstrap 3.3.7 Framework and it runs on Node js platform.

It is part of the course HY469-Modern Topics in Human Computer Interaction for the Computer Science Department
Master's Degree at the University of Crete. Further Implementation would entail full implementation of the app
and Device Synchronization (tablets, laptops, smartphones).